﻿using BeisbolGameBussines;
using BeisbolGameBussines.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Linq;
namespace BeisbolGameTest
{
    [TestClass]
    public class EquipoTest
    {

        public EquipoTest()
        {

        }

        [TestMethod]
        public void Cuando_Comienza_El_Juego_Obtengo_Estado_Inicial_Equipo_Atacante()
        {
            // Arrange
            Equipo equipo = null;

            // Act
            equipo = new Equipo();

            // Assert
            Assert.AreEqual(0, equipo.Jugadores.Count, "");
            Assert.AreEqual(0, equipo.NumeroCarreras, "");
            Assert.AreEqual("Atacante", equipo.Rol);
            Assert.AreEqual(0, equipo.ContadorOut);
        }

        [TestMethod]
        public void Cuando_Equipo_Juega_Y_Un_Jugador_Batea_Y_Obtiene_Strike_Y_Tiene_2Strikes_Previos_Y_Equipo_Tiene_0_Outs_Obtengo_1_Outs_Para_El_Equipo()
        {
            // Arrange
            IGenerateRamdon IgenerarNumeroRamdon = Substitute.For<IGenerateRamdon>();
            IgenerarNumeroRamdon.GeneratedNumberRamdon().Returns(75);
            Equipo equipoAtacanteTest = new Equipo(IgenerarNumeroRamdon);
            equipoAtacanteTest.JugadorActualEnBase0.Batear();
            equipoAtacanteTest.JugadorActualEnBase0.Batear();

            // Act
            equipoAtacanteTest.jugar();
            var numeroOutsEquipoAtacante = equipoAtacanteTest.ContadorOut;


            // Assert
            Assert.AreEqual(1, numeroOutsEquipoAtacante, "");
        }

        [TestMethod]
        public void Cuando_Equipo_Juega_Y_hay_1_Jugador_En_Base_3_Y_Otro_Jugador_Batea_Y_Equipo_Tiene_0_Carreras_Obtengo_1_Carrera()
        {
            // Arrange
            IGenerateRamdon IgenerarNumeroRamdon = Substitute.For<IGenerateRamdon>();
            IgenerarNumeroRamdon.GeneratedNumberRamdon().Returns(20);
            Equipo equipoAtacanteTest = new Equipo(IgenerarNumeroRamdon);
            Jugador jugadorEnBase3 = new Jugador();
            jugadorEnBase3.AvanzarABase();
            jugadorEnBase3.AvanzarABase();
            jugadorEnBase3.AvanzarABase();
            equipoAtacanteTest.Jugadores.Add(jugadorEnBase3);

            // Act
            equipoAtacanteTest.jugar();
            var numeroCarrerasEquipo = equipoAtacanteTest.NumeroCarreras;
            // Assert
            Assert.AreEqual(1, numeroCarrerasEquipo,"");

        }

        [TestMethod]
        public void Cuando_Equipo_Juega_Y_hay_1_Jugador_En_Base_3_Y_Otro_Jugador_En_Base_2Y_Otro_Jugador_En_Base_1_Y_Jugador_Batea_Y_Hace_HomeRun_Y_Equipo_Tiene_0_Carreras_Obtengo_4_Carreras()
        {
            // Arrange
            IGenerateRamdon IgenerarNumeroRamdon = Substitute.For<IGenerateRamdon>();
            IgenerarNumeroRamdon.GeneratedNumberRamdon().Returns(42);
            Equipo equipoAtacanteTest = new Equipo(IgenerarNumeroRamdon);
            Jugador jugadorEnBase1 = new Jugador();
            Jugador jugadorEnBase2 = new Jugador();
            Jugador jugadorEnBase3 = new Jugador();

            jugadorEnBase1.AvanzarABase();


            jugadorEnBase2.AvanzarABase();
            jugadorEnBase2.AvanzarABase();

            jugadorEnBase3.AvanzarABase();
            jugadorEnBase3.AvanzarABase();
            jugadorEnBase3.AvanzarABase();

            equipoAtacanteTest.Jugadores.Add(jugadorEnBase1);
            equipoAtacanteTest.Jugadores.Add(jugadorEnBase2);
            equipoAtacanteTest.Jugadores.Add(jugadorEnBase3);

            // Act
            equipoAtacanteTest.jugar();
            var numeroCarrerasEquipo = equipoAtacanteTest.NumeroCarreras;
            // Assert
            IgenerarNumeroRamdon.Received().GeneratedNumberRamdon();
            Assert.AreEqual(4, numeroCarrerasEquipo, "");

        }

        [TestMethod]
        public void Cuando_Equipo_Juega_Y_Tiene_2_Outs_Y_Jugador_Que_Batea_Tiene_2_Strikes_Y_Obtiene_3_Strikes_Obtengo_Numero_Outs_Para_El_Equipo_En_3_Y_Cambio_Rol_Equipo_()
        {
            // Arrange
            IGenerateRamdon IgenerarNumeroRamdon = Substitute.For<IGenerateRamdon>();
            IgenerarNumeroRamdon.GeneratedNumberRamdon().Returns(85);
            Equipo equipoAtacanteTest = new Equipo(IgenerarNumeroRamdon);
            Jugador jugadorTest1 = new Jugador(IgenerarNumeroRamdon);
            Jugador jugadorTest2 = new Jugador(IgenerarNumeroRamdon);
            
            equipoAtacanteTest.Jugadores.Add(jugadorTest1);
            equipoAtacanteTest.Jugadores.Add(jugadorTest2);

            equipoAtacanteTest.Jugadores[0].notificarOutAEquipo += equipoAtacanteTest.SumarOut;
            equipoAtacanteTest.Jugadores[1].notificarOutAEquipo += equipoAtacanteTest.SumarOut;

            equipoAtacanteTest.Jugadores[0].Batear();
            equipoAtacanteTest.Jugadores[0].Batear();
            equipoAtacanteTest.Jugadores[0].Batear();

            equipoAtacanteTest.Jugadores[1].Batear();
            equipoAtacanteTest.Jugadores[1].Batear();
            equipoAtacanteTest.Jugadores[1].Batear();

            // act
            equipoAtacanteTest.jugar();
            var numeroOuts = equipoAtacanteTest.ContadorOut;

            // Assert
            IgenerarNumeroRamdon.Received().GeneratedNumberRamdon();
            Assert.AreEqual(3, numeroOuts,"");

        }

        [TestMethod]
        public void Cuando_Equipo_Inica_Jugar_Y_Primer_Bateador_Batea_Obtengo_Mover_Bateador_Inicial_A_Base_1_YCreo_Nuevo_Jugador()
        {
            // Arrange
            var IGenerateRamdonTest = Substitute.For<IGenerateRamdon>();
            IGenerateRamdonTest.GeneratedNumberRamdon().Returns(40);
            Equipo equipoTest = new Equipo(IGenerateRamdonTest);

            // Act
            equipoTest.jugar();
            var numeroJugadores = equipoTest.Jugadores.Count();

            // Assert
            Assert.AreEqual(2, numeroJugadores, "");
        }

        [TestMethod]
        public void Cuando_Equipo_Juega_Y_Un_Jugador_Obtiene_Bola_Obtengo_Que_El_Mismo_Jugador_Sigue_Bateando()
        {
            // Arragne
            var IgeneradorNumeroAleatorio = Substitute.For<IGenerateRamdon>();
            IgeneradorNumeroAleatorio.GeneratedNumberRamdon().Returns(50);
            Equipo equipoTest = new Equipo(IgeneradorNumeroAleatorio);


            // Act
            equipoTest.jugar();
            var posicionJugadorActualBase0 = equipoTest.JugadorActualEnBase0;


            // Assert
            IgeneradorNumeroAleatorio.Received().GeneratedNumberRamdon();
            Assert.AreEqual(0, posicionJugadorActualBase0, "");

        }


        [TestMethod]
        public void Cuando_Equipo_Juega_Y_Un_Jugador_Batea_Y_Otro_Jugador_Anota_Carrera_Obtengo_Una_Carrera()
        {
            // Arrange
            var IgeneradorNumeroAleatorio = Substitute.For<IGenerateRamdon>();
            IgeneradorNumeroAleatorio.GeneratedNumberRamdon().Returns(30);
            Equipo equipoTest = new Equipo(IgeneradorNumeroAleatorio);

            equipoTest.jugar();
            equipoTest.jugar();
            equipoTest.jugar();


            // Act
            equipoTest.jugar();

            // Assert
            IgeneradorNumeroAleatorio.Received().GeneratedNumberRamdon();
            Assert.AreEqual(1, equipoTest.NumeroCarreras, "");
        }
    }
}
