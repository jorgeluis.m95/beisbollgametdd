﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeisbolGameBussines;
using BeisbolGameBussines.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
namespace BeisbolGameTest
{
    [TestCategory("Unitaria")]
    [TestClass]
    public class JugadorTest
    {

        public JugadorTest()
        {

        }

        [TestMethod]
        public void Cuando_Un_Jugador_Ingresa_Al_Juego_Obtengo_Su_Estado_Inical_En_el_Juego()
        {
            // Arrange
            Jugador player;


            // Act
            player = new Jugador();


            // Assert
            Assert.AreEqual(0, player.Posicion, "La prueba falla porque esperaba un 0 en el valor esperado, para el campo Posicion");
            Assert.AreEqual(0, player.NumeroDeStrikes, "la prueba falla porque esperaba un 0 en el valor esperado para el campo CountStrike");
            Assert.AreEqual(false, player.EstaFuera, "la prueba falla porque esperaba un false en el valor esperado para el campo IsOut");
        }

        [TestMethod]
        public void Cuando_Jugador_Batea_Obtengo_Un_Bateo()
        {
            // Arrange
            var IGenerateRamdonTest = Substitute.For<IGenerateRamdon>();
            IGenerateRamdonTest.GeneratedNumberRamdon().Returns(35);
            Jugador player = new Jugador(IGenerateRamdonTest);

            // Act
            var IsBateo = player.Batear();

            // Assert
            Assert.AreEqual(Constantes.Juego.ESTADO_BATEO, IsBateo, "La prueba falla porque se espera un valor de true en el valor esperado");
        }

        [TestMethod]
        public void Cuando_Jugador_Batea_Obrengo_Un_Strike()
        {
            // Arrange
            var IGenerateRamdonTest = Substitute.For<IGenerateRamdon>();
            IGenerateRamdonTest.GeneratedNumberRamdon().Returns(70);
            Jugador player = new Jugador(IGenerateRamdonTest);
            // Act
            var IsBateo = player.Batear();

            // Assert.s
            Assert.AreEqual(Constantes.Juego.ESTADO_STRIKE, IsBateo, "La prueba falla por que se esperaba un valor de false en el valor esperado");
        }

        [TestMethod]
        public void Cuando_Jugador_Batea_Obtengo_Cambio_De_Base_0_A_Base_1()
        {
            // Arrange
            Jugador player = new Jugador();

            // Act
            player.AvanzarABase();

            // Assert
            Assert.AreEqual(1, player.Posicion, "La prueba fallo porque se esperaba que el player inice en la base 0");
        }

        [TestMethod]
        public void Cuando_Jugador_Un_Jugador_Batea_Y_Estoy_En_Base_2_Obtengo_Cambio_De_Base_A_Base_3()
        {
            // Arrange
            Jugador playerEnBase2 = new Jugador();
            playerEnBase2.AvanzarABase();
            playerEnBase2.AvanzarABase();
            // Act
            playerEnBase2.AvanzarABase();

            // Assert
            Assert.AreEqual(3, playerEnBase2.Posicion, "La prueba fallo porque se esperaba que el player inice en la base 3");
        }


        [TestMethod]
        public void Cuando_Jugador_No_Batea_y_Tiene_O_Strikes_Obtengo_1_Strike()
        {
            // Arrange
            IGenerateRamdon IgeneradorRamdon = Substitute.For<IGenerateRamdon>();
            IgeneradorRamdon.GeneratedNumberRamdon().Returns(60);
            Jugador player = new Jugador(IgeneradorRamdon);


            // Act
            player.Batear();

            // Assert
            IgeneradorRamdon.Received().GeneratedNumberRamdon();
            Assert.AreEqual(1, player.NumeroDeStrikes, "La prueba fallo por que esperaba 1 como valor esperado");
        }


        [TestCategory("Integracion")]
        [TestMethod]
        public void Cuando_Jugador_No_Batea_yTiene_2_Strikes_Obtengo_3_Strikes_y_Out_()
        {
            // Arrange
            IGenerateRamdon IgeneradorRamdon = Substitute.For<IGenerateRamdon>();
            IgeneradorRamdon.GeneratedNumberRamdon().Returns(60);

            Jugador player = new Jugador(IgeneradorRamdon);
            player.Batear();
            player.Batear();

            // Act
            player.Batear();


            // Assert
            IgeneradorRamdon.Received().GeneratedNumberRamdon();
            Assert.AreEqual(3, player.NumeroDeStrikes);
            Assert.AreEqual(true, player.EstaFuera);

        }

        [TestMethod]
        public void Cuando_Jugador_Esta_En_Posicion_4_Obtengo_Una_Carrera_Para_El_Equipo()
        {
            // Arrange
            Jugador player = new Jugador();
            player.AvanzarABase();
            player.AvanzarABase();
            player.AvanzarABase();
            player.AvanzarABase();
            Equipo equipo = new Equipo();
            player.notificarCarrera += equipo.SumarCarrera;

            // Act
            player.AnotarCarrera();

            // Assert
            
            Assert.AreEqual(1, equipo.NumeroCarreras, "");
        }

        [TestMethod]
        public void Cuando_Jugador_NO_Batea_y_tiene_2_Strikes_Obtengo_3_Strikes_Y_Notificico_Equipo_1_Out()
        {
            // Arrange
            IGenerateRamdon IgeneradorRamdon = Substitute.For<IGenerateRamdon>();
            IgeneradorRamdon.GeneratedNumberRamdon().Returns(60);

            Jugador player = new Jugador(IgeneradorRamdon);
            player.Batear();
            player.Batear();

            Equipo equipo = new Equipo();
            player.notificarOutAEquipo += equipo.SumarOut;

            // Act
            player.NotificarOutEquipo();

            // Assert
            IgeneradorRamdon.Received().GeneratedNumberRamdon();
            Assert.AreEqual(1, equipo.ContadorOut, "");
        }

        [TestMethod]
        public void Cuando_Jugador_Batena_Y_Tiene_1_Strike_Obtengo_Numero_Strikes_En_0()
        {
            // Arrange

            IGenerateRamdon IgeneradorRamdon = Substitute.For<IGenerateRamdon>();
            IgeneradorRamdon.GeneratedNumberRamdon().Returns(60);
            Jugador jugador = new Jugador(IgeneradorRamdon);

            jugador.Batear();
            IgeneradorRamdon.GeneratedNumberRamdon().Returns(30);

            jugador.Batear();

            // Act
            jugador.ResetearStrikes();

            // Assert
            IgeneradorRamdon.Received().GeneratedNumberRamdon();
            Assert.AreEqual(0, jugador.NumeroDeStrikes, "");


        } 

        [TestMethod]
        public void Cuando_Jugador_Batea_Y_Obtiene_Bola_Y_Tiene_0_Bolas_Obtengo_1_Bola_En_Contador_de_Bolas()
        {
            // Arrange
            IGenerateRamdon IgeneradorRamdon = Substitute.For<IGenerateRamdon>();
            IgeneradorRamdon.GeneratedNumberRamdon().Returns(50);
            Jugador jugadorTest = new Jugador(IgeneradorRamdon);

            // Act
            jugadorTest.Batear();
            var contadorBolas = jugadorTest.ContadorBolas;
            // Assert
            IgeneradorRamdon.Received().GeneratedNumberRamdon();
            Assert.AreEqual(1, contadorBolas, "El contador de bolas del jugador no puede ser 1");

        }

        [TestMethod]
        public void Cuado_Jugador_Batea_Y_Obtiene_Bola_Y_Tiene_3_Bolas_Obtengo_4_Bolas_y_Jugador_Avanza_A_Base_1()
        {
            // Arrange
            IGenerateRamdon IgeneradorRamdon = Substitute.For<IGenerateRamdon>();
            IgeneradorRamdon.GeneratedNumberRamdon().Returns(50);
            Jugador jugadorTest = new Jugador(IgeneradorRamdon);

            jugadorTest.Batear();
            jugadorTest.Batear();
            jugadorTest.Batear();

            // Act
            jugadorTest.Batear();
            var posisiconJugador = jugadorTest.Posicion;
            var numeroBolas_Jugador = jugadorTest.ContadorBolas;

            // Assert
            IgeneradorRamdon.Received().GeneratedNumberRamdon();
            Assert.AreEqual(1, posisiconJugador, "");
            Assert.AreEqual(4, numeroBolas_Jugador,"");


        }

        [TestMethod]
        public void Cuado_Jugador_HACE_Bola_Y_Tiene_HAY_JUGADORES_EN_BASE_Y_2_Obtendo_Avance_Jugadores()
        {
            // Arrange
            IGenerateRamdon IgeneradorRamdon = Substitute.For<IGenerateRamdon>();
            IgeneradorRamdon.GeneratedNumberRamdon().Returns(50);
            Jugador jugadorTest = new Jugador(IgeneradorRamdon);
            Jugador jugadorTestBase1 = new Jugador(IgeneradorRamdon);

            jugadorTestBase1.AvanzarABase();

            jugadorTest.Batear();
            jugadorTest.Batear();
            jugadorTest.Batear();

            // Act
            jugadorTest.Batear();
            var posisiconJugador = jugadorTest.Posicion;
            var numeroBolas_Jugador = jugadorTest.ContadorBolas;

            // Assert
            IgeneradorRamdon.Received().GeneratedNumberRamdon();
            Assert.AreEqual(1, posisiconJugador, "");
            Assert.AreEqual(4, numeroBolas_Jugador, "");
        }



        [TestMethod]
        public void Cuando_Jugador_Batea_Y_Hace_HomeRum_Obtengo_Jugador_Anota_Una_Carrera()
        {
            // Arrange
            IGenerateRamdon IgenerarNumeroRandom = Substitute.For<IGenerateRamdon>();
            IgenerarNumeroRandom.GeneratedNumberRamdon().Returns(42);
            Jugador jugadorTest = new Jugador(IgenerarNumeroRandom);
            Equipo equipoTest = new Equipo(IgenerarNumeroRandom);
            jugadorTest.notificarCarrera += equipoTest.SumarCarrera;
            jugadorTest.Batear();

            // Act
            jugadorTest.CorrerAHome();
            var carrerasEquipo = equipoTest.NumeroCarreras;
            var posicionJugador = jugadorTest.Posicion;

            // Assert
            IgenerarNumeroRandom.Received().GeneratedNumberRamdon();
            Assert.AreEqual(1, carrerasEquipo, "");
            Assert.AreEqual(4, posicionJugador, "");
        }



        [TestMethod]
        public void Cuando_Un_Jugador_Hace_HomeRum_Y_Otro_Jugador_Esta_En_Base_2_Obtengo_Correr_Home_Para_Los_2_Jugadores_Y_2_Carreras_Para_El_Equipo()
        {
            // Arrange
            IGenerateRamdon IgenerarNumeroRandom = Substitute.For<IGenerateRamdon>();
            IgenerarNumeroRandom.GeneratedNumberRamdon().Returns(42);
            Equipo equipoTest = new Equipo();
            Jugador jugadorBateadorTest = new Jugador(IgenerarNumeroRandom);
            Jugador jugadorEnBase2Test = new Jugador();
            jugadorEnBase2Test.AvanzarABase();
            jugadorEnBase2Test.AvanzarABase();
            jugadorBateadorTest.notificarCarrera += equipoTest.SumarCarrera;
            jugadorEnBase2Test.notificarCarrera += equipoTest.SumarCarrera;
            jugadorBateadorTest.Batear();

            // Act
            jugadorBateadorTest.CorrerAHome();
            jugadorEnBase2Test.CorrerAHome();
            var numeroCarrerasEquipo = equipoTest.NumeroCarreras;

            // Assert
            IgenerarNumeroRandom.Received().GeneratedNumberRamdon();
            Assert.AreEqual(2, numeroCarrerasEquipo);
        }

        [TestMethod]
        public void Cuando_Un_Jugador_Hace_HomeRum_Y_Tengo_Jugadores_En_Base_1_2_3_Obtengo_Correr_A_Home_Para_Todos_Los_Jugadores_Y_4_Carreras_Para_El_Equipo()
        {
            // Arrange
            IGenerateRamdon IgenerarNumeroRandom = Substitute.For<IGenerateRamdon>();
            IgenerarNumeroRandom.GeneratedNumberRamdon().Returns(42);
            Equipo equipoTest = new Equipo();
            Jugador jugadorBateadorTest = new Jugador(IgenerarNumeroRandom);
            Jugador jugadorEnBase1Test = new Jugador();
            Jugador jugadorEnBase2Test = new Jugador();
            Jugador jugadorEnBase3Test = new Jugador();

            jugadorBateadorTest.notificarCarrera += equipoTest.SumarCarrera;
            jugadorEnBase1Test.notificarCarrera += equipoTest.SumarCarrera;
            jugadorEnBase2Test.notificarCarrera += equipoTest.SumarCarrera;
            jugadorEnBase3Test.notificarCarrera += equipoTest.SumarCarrera;


            jugadorEnBase1Test.AvanzarABase();

            jugadorEnBase2Test.AvanzarABase();
            jugadorEnBase2Test.AvanzarABase();

            jugadorEnBase3Test.AvanzarABase();
            jugadorEnBase3Test.AvanzarABase();
            jugadorEnBase3Test.AvanzarABase();

            
            jugadorBateadorTest.Batear();

            // Act
            jugadorBateadorTest.CorrerAHome();
            jugadorEnBase1Test.CorrerAHome();
            jugadorEnBase2Test.CorrerAHome();
            jugadorEnBase3Test.CorrerAHome();

            var carrerasEquipo = equipoTest.NumeroCarreras;

            // Assert
            Assert.AreEqual(4, carrerasEquipo, "");

        }
    }
}
