﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeisbolGameBussines
{
    public class Constantes
    {

        public class Jugador
        {
            public const int LIMITE_INFERIOR_BATEO = 0;
            public const int LIMITE_SUPERIOR_BATEO = 39;
            public const int LIMITE_INFERIOR_BOLA = 46;
            public const int LIMITE_SUPERIOR_BOLA = 55;
            public const int LIMITE_INFERIOR_STRIKE = 56;
            public const int LIMITE_SUPERIOR_STRIKE = 100;
            public const int LIMITE_INFERIOR_HOMERUN = 40;
            public const int LIMITE_SUPERIOR_HOMERUN = 45;
            public const int PASO_AVANCE_JUGADOR = 1;
            public const int VALOR_LLEGA_BASE = 4;
            public const bool ESTOY_EN_HOME = true;
            public const bool NO_ESTOY_EN_HOME = true;


        }

        public class Juego
        {
            public const int POSICION_ICIAL_JUGADOR = 0;
            public const bool PUEDE_AVANZAR = false;
            public const int PASO_INCREMENTO_NUEVO_STRIKE = 1;
            public const int PASO_INCREMENTO_NUEVO_OUT = 1;
            public const int CANTIDAD_MAXIMA_STRIKE_PORA_JUGADOR = 3;
            public const int CANTIDAD_MAXIMA_OUTS_PORA_EQUIPO = 3;
            public const int PASO_INCREMENTO_ANOTACION_NUEVA_CARRERA = 1;
            public const string ESTADO_BOLA = "BOLA";
            public const string ESTADO_BATEO = "BATEO";
            public const string ESTADO_STRIKE = "STRIKE";
            public const string ESTADO_HOMERUN = "HOMERUN";
            public const int PASO_INCREMENTO_BOLAS = 1;
            public const int VALOR_pOSICION_BASE1 = 1;
            public const int VALOR_pOSICION_BASE2 = 2;
            public const int VALOR_pOSICION_BASE3 = 3;
            public const int VALOR_pOSICION_BASE4 = 4;
            public const int VALOR_pOSICION_BASE0 = 0;


        }


    }
}
