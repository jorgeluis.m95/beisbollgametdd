﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeisbolGameBussines.Interfaces;

namespace BeisbolGameBussines
{
    public class Equipo
    {

        private IGenerateRamdon _generarNumeroAleatorio;

        public string Nombre { get; private set; }

        public string Rol { get; private set; }

        public int NumeroCarreras { get; private set; }
        public int ContadorOut { get; private set; }
        public List<Jugador> Jugadores { get; set; }

        public int bolaRandom = 0;

        public Jugador JugadorActualEnBase0 { get; set; }

        public Equipo()
        {
            this.Nombre = "Nacional";
            this.NumeroCarreras = 0;
            this.ContadorOut = 0;
            this.Rol = "Atacante";
            Jugadores = new List<Jugador>();

        }

        public Equipo(IGenerateRamdon generarNumeroRandom) : this()
        {
            _generarNumeroAleatorio = generarNumeroRandom;
            JugadorActualEnBase0 = new Jugador(_generarNumeroAleatorio);
        }

        public void SumarCarrera(object sender, EventArgs args)
        {
            this.NumeroCarreras = this.NumeroCarreras + Constantes.Juego.PASO_INCREMENTO_ANOTACION_NUEVA_CARRERA;
        }

        public void SumarOut(object sender, EventArgs args)
        {
            this.ContadorOut = this.ContadorOut + Constantes.Juego.PASO_INCREMENTO_NUEVO_OUT;
            var jugadorEliminar = sender as Jugador;
            SacarJugadorDelJuego(jugadorEliminar);
            this.JugadorActualEnBase0 = IngresarNuevoJugadorAlJuego();
        }

        private void ResultadoBateo(string resultadoBateo, Jugador jugadorActual)
        {
            switch (resultadoBateo)
            {
                case Constantes.Juego.ESTADO_BATEO:
                    JugadorActualEnBase0 = IngresarNuevoJugadorAlJuego();
                    break;
                case Constantes.Juego.ESTADO_HOMERUN:
                    JugadorActualEnBase0 = IngresarNuevoJugadorAlJuego();
                    break;
                case Constantes.Juego.ESTADO_BOLA:
                    var resultadoBateador = JugadorActualEnBase0.Batear();
                    ResultadoBateo(resultadoBateador, JugadorActualEnBase0);
                    break;
                case Constantes.Juego.ESTADO_STRIKE:
                    jugadorActual.Batear();
                    break;

            }
        }

        public void jugar()
        {
                var resultadoBateo = "";
                
                JugadorActualEnBase0.notificarOutAEquipo += SumarOut;
                this.Jugadores.Add(JugadorActualEnBase0);
                PrepararJugadoresParaJugar();
                resultadoBateo = JugadorActualEnBase0.Batear();
                ResultadoBateo(resultadoBateo, JugadorActualEnBase0);

            
        }

        private void PrepararJugadoresParaJugar()
        {
            if (this.Jugadores.Count > 0)
            {
                foreach (var jugador in Jugadores)
                {
                    JugadorActualEnBase0.Batee+=jugador.RecibirNotificacionDeBateo;
                    JugadorActualEnBase0.HiceHomeRun += jugador.RecibirNotificacionDeHomeRun;
                    jugador.notificarCarrera += SumarCarrera;
                }
            }
        }

        private Jugador IngresarNuevoJugadorAlJuego()
        {
            var nuevoJugador = new Jugador(_generarNumeroAleatorio);
            return nuevoJugador;
        }
        private void SacarJugadorDelJuego(Jugador jugadorEliminar)
        {
            this.Jugadores.Remove(jugadorEliminar);
        }

    }
}
