﻿using System;
using BeisbolGameBussines.Interfaces;

namespace BeisbolGameBussines
{
    public class Jugador
    {


        private readonly IGenerateRamdon generateNumberRandomService;

        public event EventHandler notificarCarrera;

        public event EventHandler notificarOutAEquipo;
        public event EventHandler Batee;
        public event EventHandler HiceHomeRun;
        public int Posicion { get; private set;}
        public int NumeroDeStrikes { get; private set; }
        
        public bool EstaFuera { get; private set; }

        public int ContadorBolas { get; private set; }

        public Jugador()
        {
            Posicion = 0;
            NumeroDeStrikes = 0;
            EstaFuera = false;
            generateNumberRandomService = new GeneratedRamdon();
        }

        public Jugador(IGenerateRamdon _generateNumberRandom) : this()
        {
            generateNumberRandomService = _generateNumberRandom;

        }

        public string Batear()
        {
            string estadoBateo = "";
            int bola = generateNumberRandomService.GeneratedNumberRamdon();
            if (bola >= Constantes.Jugador.LIMITE_INFERIOR_BATEO && bola <= Constantes.Jugador.LIMITE_SUPERIOR_BATEO)
            {
                //AvanzarABase();
                NotificacionDeBateo();
                estadoBateo = Constantes.Juego.ESTADO_BATEO;
            }

            if (bola >= Constantes.Jugador.LIMITE_INFERIOR_HOMERUN && bola <= Constantes.Jugador.LIMITE_SUPERIOR_HOMERUN)
            {
                NotificacionDeHomeRun();
                estadoBateo = Constantes.Juego.ESTADO_HOMERUN;
            }

            if (bola >= Constantes.Jugador.LIMITE_INFERIOR_BOLA && bola <= Constantes.Jugador.LIMITE_SUPERIOR_BOLA)
            {
                ContarBolas();
                if (ValidarSiTengo4Bolas())
                {
                    AvanzarABase();
                }
                estadoBateo = Constantes.Juego.ESTADO_BOLA;
            }

            if (bola >= Constantes.Jugador.LIMITE_INFERIOR_STRIKE && bola <= Constantes.Jugador.LIMITE_SUPERIOR_STRIKE)
            {
                ContarStrikes();
                ValidarSiEstoyFueraDelJuego();
                estadoBateo = Constantes.Juego.ESTADO_STRIKE;
            }

            return estadoBateo;
        }

        public void AvanzarABase()
        {
            this.Posicion = this.Posicion + Constantes.Jugador.PASO_AVANCE_JUGADOR;
            if (ValidarSiEstoyEnHome())
            {
                AnotarCarrera();
            }
        }

        public void RecibirNotificacionDeBateo(object sender, EventArgs args)
        {
            AvanzarABase();
        }

        public void RecibirNotificacionDeHomeRun(object sender, EventArgs args)
        {
            CorrerAHome();
        }

        public void NotificacionDeHomeRun()
        {
            EventHandler handler = HiceHomeRun;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public void NotificacionDeBateo()
        {
            EventHandler handler = Batee;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public void AnotarCarrera()
        {
            EventHandler handler = notificarCarrera;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public void NotificarOutEquipo()
        {
            EventHandler handlerNotificarOutEquipo = notificarOutAEquipo;
            if (handlerNotificarOutEquipo != null)
            {
                handlerNotificarOutEquipo(this, EventArgs.Empty);
            }
        }

        private bool ValidarSiEstoyEnHome()
        {
            if (this.Posicion == Constantes.Jugador.VALOR_LLEGA_BASE)
            {
                return Constantes.Jugador.ESTOY_EN_HOME;
            }
            else
            {
                return !Constantes.Jugador.NO_ESTOY_EN_HOME;
            }
        }
        private void ValidarSiEstoyFueraDelJuego()
        {
            if (this.NumeroDeStrikes == Constantes.Juego.CANTIDAD_MAXIMA_STRIKE_PORA_JUGADOR)
            {
                NotificarOutEquipo();
                this.EstaFuera = true;
            }
        }

        private void ContarStrikes()
        {
            this.NumeroDeStrikes += Constantes.Juego.PASO_INCREMENTO_NUEVO_STRIKE;
        }

        public void ResetearStrikes()
        {
            this.NumeroDeStrikes = 0;
        }
        private void ContarBolas()
        {
            this.ContadorBolas += Constantes.Juego.PASO_INCREMENTO_BOLAS;
        }
        private bool ValidarSiTengo4Bolas()
        {
            if(this.ContadorBolas == 4)
            {
                return true;
            }else
            {
                return false;
            }
        }
        public void CorrerAHome()
        {
            switch (this.Posicion)
            {
                case Constantes.Juego.VALOR_pOSICION_BASE0:
                    AvanzarABase();
                    AvanzarABase();
                    AvanzarABase();
                    AvanzarABase();
                    break;
                case Constantes.Juego.VALOR_pOSICION_BASE1:
                    AvanzarABase();
                    AvanzarABase();
                    AvanzarABase();
                    break;
                case Constantes.Juego.VALOR_pOSICION_BASE2:
                    AvanzarABase();
                    AvanzarABase();
                    break;
                case Constantes.Juego.VALOR_pOSICION_BASE3:
                    AvanzarABase();
                    break;
                default:
                    break;
            }
        }


    }
}
